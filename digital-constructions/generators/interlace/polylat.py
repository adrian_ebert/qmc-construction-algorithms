from __future__ import print_function
import numpy as np

####
## (C) Dirk Nuyens, KU Leuven, 2014,2015,2016,...
## See https://people.cs.kuleuven.be/~dirk.nuyens/qmc4pde/

def ffac(x, n):
    """Falling factorial / Pochammer / x to the n falling."""
    r = 1
    for t in range(n):
        r *= (x-t)
    return r

def base2interlace_col(alpha, Cs_col):
    """Interlace generating matrices of a digital net in base 2.
    The original alpha*s m-by-m generating matrices must be given in column format.
    Returns m*alpha bit integers which represent the columns with lsb representing column 0 and row 0."""
    m = len(Cs_col[0])
    d = len(Cs_col)
    s = d // alpha
    Csinterlaced_col = [ [ 0 for c in range(m) ] for j in range(s) ]
    for j in range(alpha*s):    # j: dimension in alpha*s set
        for c in range(m):      # c: column
            for i in range(m):  # i: bits
                Csinterlaced_col[j//alpha][c] |= ((Cs_col[j][c] >> i) & 1) << (alpha*i+j%alpha)
    return Csinterlaced_col

def parity(a):
    parity = 0
    while a:
        parity = ~parity
        a = a & (a - 1)
    return -parity

# naive way of bitreverse, duplicated in digitalseq_b2g.py
def bitreverse(a, m=None):
    a_bin = "{0:b}".format(a)
    a_m = len(a_bin)
    if m == None: m = a_m
    a_rev = int(a_bin[::-1], 2) << max(0, m - a_m)
    return a_rev

def poly2genmatrix(z, p):
    """Calculate row/column representation for m x m generator matrix for z(X)/p(X) over Z_2."""
    z = int(z)
    p = int(p)
    if not (p & 1): raise ValueError("Error: your polynomial p does not seem to be primitive.")
    m = floorlog2(p)
    w = 0
    Cs = []
    for i in range(1,m+1):
        z_mmi = (z & (1 << m-i)) >> (m-i)
        w = (z_mmi ^ parity(w & p)) << (m-1) | (w >> 1)
    Cs.append(w)
    for j in range(2,m+1):
        w = parity(w & p) << (m-1) | (w >> 1)
        Cs.append(w)
    return Cs

def floorlog2(p):
    m = 0
    while p >> (m+1): m = m + 1
    return m

"""Default primitive polynomials over Z_2 for increasing degree."""
default_base2_polys = \
    {
        2: 7,
        3: 13,
        4: 25,
        5: 41,
        6: 97,
        7: 193,
        8: 333,
        9: 789,
        10: 1153,
        11: 2561,
        12: 5127, 
        13: 13825, 
        14: 24345, 
        15: 49153, 
        16: 86231, 
        17: 147457, 
        18: 393211, 
        19: 933889, 
        20: 1179649, 
        21: 2621441, 
        22: 6291457, 
        23: 8650753, 
        24: 28311553, 
        25: 37748737, 
        26: 111937197, 
        27: 239075329, 
        28: 286338323, 
        29: 671088641, 
        30: 1432791463
    }

def fastinterlacedpoly2rank1spod(s_max, alpha, b, m=None, p=None, 
                                 d1=1, a1=0, a2=1, a3=0,
                                 Cwalsh=1, tau_alpha=None,
                                 power_2_fft=False,
                                 my_fft=None, my_ifft=None, 
                                 float_t=float, float_dtype=np.float):
    
    # TODO: specialize for product case when d1==0
    
    import os
    from numpy import zeros, ones, r_
    index_t = 'i' # integer representations here are limited to 32 bit (the methods above work for arbitrary length though)

    if a3: raise ValueError("Sorry, lognormal case not implemented yet for polylat, a3 should be zero for now...")

    # for multiprecision we need some small patches:
    def mp_real_inplace(a):
        for i in range(len(a)): a[i] = a[i].real
        return a
    if float_t != float:
        real_possiblyinplace = mp_real_inplace
        import myfft
        fft  = myfft.fft_mp
        ifft = myfft.ifft_mp
        power_2_fft = True
        import mpmath
        sqrt = mpmath.sqrt
        print("** Using mp fft and ifft (in power of 2) **")
    else: 
        real_possiblyinplace = np.real
        from numpy.fft import fft, ifft
        import math
        sqrt = math.sqrt
    if my_fft : fft = my_fft
    if my_ifft: ifft = my_ifft
    
    if p and m:
        if m != floorlog2(p): raise ValueError("If you specify both p and m then m should be floor(log2(p))...")
    elif p and not m: m = floorlog2(p)
    elif m: p = default_base2_polys[m]
    else: raise ValueError("You need to specify either the polynomial p or the power-of-2 m to get N=2^m points...")
    if m > 30: raise ValueError("Sorry but m is currently limited to 30.")

    # number of points in the interlaced polynomial lattice rule is n = 2**m
    n = 2**m
    # the size of the fft is N = n - 1 as the multiplicative group of
    # Z_2[x]/p(x) is Z_2[x]/p(x) \ {0} for p irreducible but, if using base 2
    # fft (eg for multiprecision calcs) then the size of the fft is N =
    # 2**(m+1), obtained by symmetry
    N = 2**(m+1) if power_2_fft else n-1
    
    if tau_alpha: tau = tau_alpha
    else: tau = sqrt(float_t(2**(alpha*(alpha-1))))
    if Cwalsh: C = Cwalsh
    else: C = float_t(1)

    def gamma(j, v): # Eqs (3.17) and (3.32) from Dick, Kuo, Le Gia, Nuyens & Scwab (2014)
        # we cast j to float_t before asking the value of b_j such that we certainly
        # get it in the correct precision
        t = C * tau * (a2 * b(float_t(j)))**v
        if v == alpha: t *= 2
        return t

    print("spod construction for p=%d, m=%d, s_max=%d, alpha=%d, b=%g,...,%g, d1=%g, a1=%g, a2=%g, a3=%g, Cwalsh=%g, tau_alpha=%g" \
            % (p,m,s_max,alpha,b(1),b(s_max),d1,a1,a2,a3,C,tau))

    # NB: it looks funny to say x = zeros(...) and then to say x[:] =
    # float_t(0), but this is to get a correct zero array in case of
    # multiprecision
    U = zeros((n, alpha*s_max+1), order='F', dtype=float_dtype); U[:] = float_t(0)
    U[:,0] = float_t(1) / ffac(a1, a1)**d1
    V = zeros(N+1, dtype=float_dtype); V[:] = float_t(0)
    W = zeros(N+1, dtype=float_dtype); W[:] = float_t(0)
    X = zeros((n, alpha*s_max+1), order='F', dtype=float_dtype); X[:] = float_t(0)
    # note: we use one-based indexing for the second index of the X array

    datadir = os.path.join(os.path.dirname(__file__), 'polylat-data')
    kernel_a1 = np.fromfile(os.path.join(datadir, "a1_b2_m%d_p%d.txt" % (m, p)), dtype=index_t, sep="\n")
    zmap = np.fromfile(os.path.join(datadir, "z_b2_m%d_p%d.txt" % (m, p)), dtype=index_t, sep="\n")

    omega = zeros(N+1, dtype=float_dtype); omega[:] = float_t(0)
    omega[0] = 1 / float_t(2**alpha - 2)
    omega[1:n] = 1 / float_t(2**alpha-2) - ((2**alpha-1) * 2**(kernel_a1*float_t(1-alpha))) / float_t(2**alpha-2)
    if power_2_fft: omega[-n+1:] = omega[1:n]
    fft_omega = fft(omega[1:])

    w = zeros((s_max,alpha), index_t)
    z = zeros((s_max,alpha), index_t)
    E = zeros(s_max, dtype=float_dtype); E[:] = float_t(0)

    # below we write i0 for a 0-based index and use i for the 1-based index, i = i0 + 1
    for s0 in range(s_max):
        s = s0 + 1
        print("s=%d" % s)

        V[:] = float_t(1)

        # calculate W_s
        W[:n] = float_t(0)
        for ell in range(1, alpha*s+1):
            X[:,ell] = float_t(0)
            for v in range(max(1, ell-alpha*(s-1)), min(alpha, ell)+1):
                X[:,ell] += gamma(s, v) * ffac(ell+a1, v)**d1 * U[:,ell-v]
            W[:n] += X[:,ell]

        for t0 in range(alpha):
            t = t0 + 1
            
            Z = fft(V[1:]*W[1:]) # V and W are padded with zero's in case of a power of 2 fft
            Z *= fft_omega
            YY = ifft(Z)[:n-1]
            YY = real_possiblyinplace(YY) # for mp the real happens in-place

            delta = YY.argmin() # 0 <= delta < 2^m-1; this is the power of X we need to take
            if t0 + s0 == 0: print("noise = %f" % (YY[0] - YY[delta])); delta = 0
            z[s0,t0] = zmap[delta]
            w[s0,t0] = delta 
            print("(%3d,%d) -> %10d %10d %20g" % (s, t, delta, zmap[delta], YY[delta]))

            # update V, based on choice of generator w1
            w1 = delta + 1
            V[:n] *= (1 + omega[r_[0, w1:0:-1, n-1:w1:-1]])

        # update from U_{s-1} to U_s
        for ell in range(1, alpha*s+1):
            U[:,ell] += (V[:n] - 1) * X[:,ell]

        # calculate E_s for reference
        E[s0] = U[:,1:].sum() / n
        print("error for s=%3d: %26g" % (s, E[s0]))

    # z now are the integer representations of the polynomials of the s*alpha rule as integers
    # w are the powers of the generator X, thus X^w mod P(x) = z(X)
    Cs = [ poly2genmatrix(zj, p) for zjs in z for zj in zjs ] # list of list of length alpha*s
    Bs = base2interlace_col(alpha, Cs)

    return (Bs, Cs, z, E, p, w, V, W, U)

