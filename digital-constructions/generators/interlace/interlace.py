#!/usr/bin/env python

####
## (C) Dirk Nuyens, KU Leuven, 2016,...
##

# to interlace generating matrices which are in column format
# one matrix per row
# call this script with as first argument the interlacing factor alpha
# and as second argument the filename of the file containing the original generating matrices

from polylat import base2interlace_col
from sys import argv

def save_to_file(filename, Bs):
    f = open(filename, 'w')
    for mat in Bs:
        for col in mat:
            print(col, end=' ', file=f)
        print(file=f)
    f.close()

alpha = int(argv[1])
Csfilename = argv[2]

f = open(Csfilename)
Cs = [ list(map(int, line.split())) for line in f ]

Bs = base2interlace_col(alpha, Cs)
Bs53 = [ [ Bjc % 2**53 for Bjc in Bj ] for Bj in Bs ]
Bs64 = [ [ Bjc % 2**64 for Bjc in Bj ] for Bj in Bs ]

if len(Csfilename) > 6 and Csfilename[-6:] == 'Cs.col':
    fn = Csfilename[:-6]
else:
    fn = Csfilename

save_to_file('%salpha%d_Bs53.col' % (fn, alpha), Bs53)
save_to_file('%salpha%d_Bs64.col' % (fn, alpha), Bs64)
save_to_file('%salpha%d_Bs.col' % (fn, alpha), Bs)
