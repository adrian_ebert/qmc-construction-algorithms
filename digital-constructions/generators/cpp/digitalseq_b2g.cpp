#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <string>
#include <cstring>
#include <random>
#include <algorithm>

////
// (C) Dirk Nuyens, KU Leuven, 2014,2015,2016,2017,...

#ifndef VERSION
#define VERSION "<unknown version>"
#endif

#include "digitalseq_b2g.hpp"
#include "cmdline_parser.hpp"

/// Print a vector to a stream, delemited by spaces.
template <typename T>
std::ostream& operator<<(std::ostream& s, const std::vector<T>& v)
{
    auto w = s.width();
    for(const auto& e : v) s << std::setw(w) << e << ' ';
    return s;
}

/// Convert int to string of bit representation in reverse order (msb first).
template <typename INT>
std::string int_to_bitstring(const INT a, int t=0, bool reversed=false)
{
    const int p = std::numeric_limits<INT>::digits; // assuming this is a base 2 type
    if(t == 0) t = p;
    std::string out = "";
    for(int i = 0; i < t; ++i)
        if(reversed && ((INT(1) << (p-i-1)) & a)) out += '1';
        else if(!reversed && ((INT(1) << i) & a)) out += '1';
        else out += '0';
    return out;
}

/// Convert integer column encoded generating matrix to binary string (having m columns and bitdepth t).
template <typename INT>
std::string matrix_to_bitstring(int t, int m, INT* it, bool reversed=false)
{
    const int p = std::numeric_limits<INT>::digits; // assuming this is a base 2 type
    std::string out = "";
    for(int i = 0; i < t; ++i) { // iterate over bits
        for(int j = 0; j < m; ++j)  // iterate over columns
            if(reversed && (*(it + j) & (INT(1) << (p-i-1)))) out += " 1 ";
            else if(!reversed && (*(it + j) & (INT(1) << i))) out += " 1 ";
            else out += " 0 ";
        out += '\n';
    }
    return out;
}

int main(int argc, char* argv[])
{
    typedef std::uint64_t       uint_t;
    typedef long double         float_t;
    typedef std::mt19937_64     rng_t;

    unsigned m = 0, s = 0;
    std::string filename;
    std::uint32_t state = 0;
    bool show_help      = false;
    bool nb             = false;
    bool print_matrices = false;
    bool print_binary   = false;
    bool print_integer  = false;
    bool print_nofloat  = false;
    unsigned seed       = rng_t::default_seed; // currently assuming unsigned is 64 bit on your platform
    bool scramble       = false;
    bool shift          = false;
    unsigned t2         = 0;
    // bind command line arguments to variables:
    util::cmdline_parser::args_t args;
    args["-s"       ] = { s,              "number of dimensions" };
    args["-m"       ] = { m,              "generate 2^m points" };
    args["-f"       ] = { filename,       "load matrices from file" };
    args["-seed"    ] = { seed,           "seed for the Mersenne Twister to generate random scrambles and shifts, see -shift and -scramble" };
    args["-scramble"] = { scramble,       "(randomly) scramble matrices M_j; with scrambledCs_j = M_j * C_j, see -seed and -shift" };
    args["-t2"      ] = { t2,             "bit depth for the scrambling matrices (nb of rows of scrambling matrix), see -scramble\n"
                                          "if t2=0 (default) then the same bit depth as from the generating matrices is used" };
    args["-shift"   ] = { shift,          "(randomly) digitally shift points, see -seed and -scramble" };
    args["-h"       ] = { show_help,      "show this help message" };
    args["-state"   ] = { state,          "set the state of the generator to the given value" };
    args["-nb"      ] = { nb,             "show index of point (state) in output" };
    args["-matrices"] = { print_matrices, "print generating matrices (decimal or binary); exits afterwards" };
    args["-binary"  ] = { print_binary,   "print points or matrices in binary" };
    args["-integer" ] = { print_integer,  "print points in binary" };
    args["-nofloat" ] = { print_nofloat,  "do not print points as float" };
    // and provide positional arguments for backward compatibility:
    util::cmdline_parser::pos_args_t pos_args;
    pos_args.push_back( &args["-s"] );
    pos_args.push_back( &args["-m"] );
    pos_args.push_back( &args["-f"] );
    // parse the command line:
    bool ok = util::cmdline_parser::parse_args(argv+1, argv+argc, args, pos_args);

    // print help message if asked or if command line was not ok
    if(show_help || !ok)
    {
        if(!ok) std::cerr << "\n";
        std::cerr << "Generate points of a digital sequence, print to stdout (diagnostics on stderr).\n"
                     "(C) Dirk Nuyens, 2017, " << VERSION << ".\n\n"
                     "Short synopsis: " << argv[0] << " [s [m]] [file] [< mat]\n"
                     "  s             number of dimensions to generate\n"
                     "  m             generate 2^m points of this sequence\n"
                     "  [file]        generating matrices read from file\n"
                     "  [< mat]       read generating matrices from stdin\n"
                     "If m is not given m is deduced from parsing the generating matrices, "
                     "similar for s.\n\n"
                     "Full list of arguments:\n";
        util::cmdline_parser::print_args_help(std::cerr, args);
        std::cerr << "\nCommand line seen: " << util::cmdline_parser::args_detailed_print(args) << "\n";
        if(!ok) std::cerr << "\nERROR parsing command line, please check messages at the top...\n";
        return 1;
    }

    bool print_float = !print_nofloat;

    // load generating matrices from stdin or from file
    std::cerr << "s=" << s << " m=" << m << " filename=";
    std::istream* isptr(&std::cin); // use stdin if no filename was provided
    std::ifstream ifs;
    if(filename.size()) {
        ifs.open(filename);
        if(!ifs.is_open()) {
            std::cerr << " :: Error: couldn't open " << filename << "\n";
            return 1;
        }
        isptr = &ifs;
    } else {
        filename = "<stdin>";
    }
    std::cerr << filename << std::endl;
    bool print_info_again = (s == 0) | (m == 0);

    // load generating matrices, scramble and shift if needed
    auto Cs = qmc::load_generating_matrices(*isptr, s, m);
    auto t  = qmc::bit_depth(Cs.begin(), Cs.end());
    if(t2 == 0) t2 = t;

    std::vector<std::uint64_t> shifts(s);
    std::vector<std::uint64_t> scrambled_Cs = Cs;
    std::vector<std::uint64_t> M(s * t);

    // generate random scrambles and shift if necessary
    if(shift || scramble)
    {
        rng_t rng(seed);
        // we always generate the random numbers for the digital shift first
        // even if we are not shifting, just to be able to compare all possible
        // combinations of the effect of scrambling and shifting
        std::generate_n(shifts.begin(), s, rng); // first generate
        if(!shift) std::fill_n(shifts.begin(), s, 0); // reset to zero if not needed
        if(scramble) {
            qmc::generate_random_linear_scramble(s, t2, t, M.begin(), rng);
            qmc::scramble(s, t, m, M.begin(), Cs.begin(), scrambled_Cs.begin());
        }
    }

    // construct generator
    qmc::digitalseq_b2g<float_t, uint_t> g(s, m, scrambled_Cs.begin(), shifts.begin());
    g.set_state(state);

    if(print_info_again)
        std::cerr << "=> s=" << s << " m=" << m << " filename=" << filename << "\n";

    std::cerr << "original bit depth / nb of rows = " << t
              << ", bit depth after scrambling = " << t2 << "\n"
              << "Csr_hash = " << std::hex << std::showbase << g.Csr_hash
	          << ", shfr_hash = " << g.shfr_hash << std::dec << std::noshowbase << "\n"
              << "state k = " << g.k << "\n";

    if(print_matrices)
    {
        for(unsigned j = 0; j < s; ++j) {
            std::cout << "Dimension " << j+1 << ":";
            std::cout << " [ ";
            for(unsigned i = 0; i < m; ++i)
            std::cout << qmc::bitreverse(g.Csr[j*m+i]) << " ";
            std::cout << "]";
            if(shift) std::cout << ", shift = base10:" << std::setw(20) << std::setfill('0') << std::right << shifts[j] << std::setfill(' ')
                                << " = base2:" << int_to_bitstring(shifts[j]);
            std::cout << "\n";
            if(print_binary) {
                // quite some work to make this print neatly in case of scrambling...
                std::string scrambled_Cs_string(matrix_to_bitstring(t2, m, &g.Csr[j*m], true)), scrambled_Cs_line,
                            Cs_string, Cs_line,
                            M_string, M_line;
                if(scramble) {
                    Cs_string = matrix_to_bitstring(t, m, &Cs[j*m]);
                    M_string  = matrix_to_bitstring(t2, t, &M[j*t]);
                }
                auto scrambled_Cs_length = scrambled_Cs_string.find_first_of('\n'),
                     Cs_length           = Cs_string.find_first_of('\n'),
                     M_length            = M_string.find_first_of('\n');
                std::istringstream scrambled_Cs_ss(scrambled_Cs_string),
                                   Cs_ss(Cs_string),
                                   M_ss(M_string);
                unsigned row = 0;
                while(bool(std::getline(scrambled_Cs_ss, scrambled_Cs_line)) |
                      bool(std::getline(Cs_ss, Cs_line)) |
                      bool(std::getline(M_ss, M_line)))
                {
                    ++row;
                    std::cout << std::setw(scrambled_Cs_length) << scrambled_Cs_line
                              << ((scramble & (row == t2/2)) ? " = " : "   ")
                              << std::setw(M_length) << M_line
                              << ((scramble & (row == t2/2)) ? " * " : "   ")
                              << std::setw(Cs_length) << Cs_line << '\n';
                }
            }
        }
        return 0;
    }

    // generate the points and print them to stdout
    int prec = std::numeric_limits<float_t>::max_digits10;
    std::cout.precision(prec);

    // separate out printing the index of the point by a lambda function:
    auto print_nb_fun = [&]() { if(nb) std::cout << std::setw(5) << std::right << g.k << "      "; };
    int int_w = 20, bin_w = g.t, float_w = prec+4;
    if(shift) bin_w += 3; // we put ellipsis when we have digitally shifted
    int print_w = std::max({ print_integer ? int_w : 0, print_binary ? bin_w : 0, print_float ? float_w : 0});

    for(; g != g.end(); ++g) {
        if(print_integer) {
            print_nb_fun();
            std::cout << std::setw(print_w) << std::right << std::setfill('0') << g.cur << std::setfill(' ') << "\n";
        }
        if(print_binary) {
            print_nb_fun();
            // NOTE: we print the `g.t` most-significant bits where t was determined
            // by the digital sequence generator on construction as the maximum
            // number of bits necessary to represent the columns of the generating matrices
            for(unsigned j = 0; j < s; ++j)
                std::cout << std::setw(print_w) << std::left
                          << (int_to_bitstring(g.cur[j] ^ g.shfr[j], g.t, true) + (shift ? "..." : "")) << " ";
            std::cout << "\n";
        }
        if(print_float) {
            print_nb_fun();
            std::cout << std::setw(print_w) << std::left << *g << "\n";
        }
    }

    return 0;
}
