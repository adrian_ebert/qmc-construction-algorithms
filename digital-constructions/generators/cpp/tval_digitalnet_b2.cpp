// Compilation via: clang++ -O3 -Wall -std=c++11 -I ~/Documents/C++/boost_1_63_0/ tval_digitalnet_b2.cpp -o tval_digitalnet_b2
// (C) Adrian Ebert, KU Leuven, 2019

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <random>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <string>
#include <cstring>
#include <algorithm>
#include <stdlib.h>

#include <boost/multiprecision/cpp_int.hpp>             // boost multiprecision integer library
#include "digitalseq_b2g.hpp"                           // the C++ digital sequence point generator

/** Determine the position of the most significant bit of a std::uint64_t.
  *
  * Note that when interpreting x as a polynomial mod 2 of degree < 64
  * then msb_pos corresponds to the degree of the polynomial given by x. */
int msb_pos(std::uint64_t v)
{
    static const int DeBruijnBitPosition[64] =
    {
    0 , 47,  1, 56, 48, 27,  2, 60, 57, 49, 41, 37, 28, 16,  3, 61,
    54, 58, 35, 52, 50, 42, 21, 44, 38, 32, 29, 23, 17, 11,  4, 62,
    46, 55, 26, 59, 40, 36, 15, 53, 34, 51, 20, 43, 31, 22, 10, 45,
    25, 39, 14, 33, 19, 30,  9, 24, 13, 18,  8, 12,  7,  6,  5, 63
    };

    v |= (v >> 1);
    v |= (v >> 2);
    v |= (v >> 4);
    v |= (v >> 8);
    v |= (v >> 16);
    v |= (v >> 32);

    return DeBruijnBitPosition[(std::uint64_t)(v * 0x03f79d71b4cb0a89UL) >> 58];
}

// Function to determine the binomial coefficient n choose k in arbitrary precision:
using namespace boost::multiprecision;
cpp_int binom(std::int64_t n, std::int64_t k)
{
    if (k > n) return 0;
    int64_t k_ = (k < (n-k)) ? k : (n-k);
    cpp_int res = 1;
    for (int j=0; j < k_; ++j)
    {
        res *= n - j;
        res /= j + 1;
    }
    return res;
}

// Calculate the coefficients of the degree-m polynomial Q_m(x) (see notes):
std::vector<cpp_int> Q_m(std::int64_t m, std::int64_t s)
{
    std::vector<cpp_int> w(m+1,0);
    w[0] = 1; w[1] = s;
    for (int a=2; a <= m; a++) {
        int min = a ^ ((s ^ a) & -(s < a));
        for (int k=1; k <= min; k++) {
            w[a] += (binom(s,k) * binom(a-1,k-1)) << (a-k);
        }
    }
    return w;
}

/** Calculate the coefficients of the degree-m polynomial obtained by the set bits
    of the digital net points (see notes): */
std::vector<cpp_int> poly_pts(unsigned& m, unsigned& s, std::string file)
{
    std::vector<cpp_int> w(m+1,0); w[0] = 1;
    // Load file containing the generating matrices:
    std::istream* isptr(&std::cin);
    std::ifstream ifs;
    if(file.size()) {
        ifs.open(file);
        if(!ifs.is_open()) {
            std::cerr << " :: Error: couldn't open " << file << "\n";
            return w;
        }
        isptr = &ifs;
    }
    std::cerr << "Loading digital net from: " << file << std::endl;
    // Load generating matrices and construct generator:
    auto Cs = qmc::load_generating_matrices(*isptr, s, m);
    qmc::digitalseq_b2g<long double, std::uint64_t> gen(s, m, Cs.begin());
    for (int l=0; l < 1 << m; l++) {
        std::vector<cpp_int> w_pt(m+1,0); w_pt[0] = 1;
        for (int j=0; j < s; j++) {
            int msb = msb_pos((gen.cur[j] >> (64-m)) << (64-m));
            if (msb > 0){
                for (int i=m; i >= 64-msb; i--) {
                    w_pt.at(i) -= w_pt.at(i+msb-64);
                }
            }
        }
        for (int i=1; i <= m; i++) w[i] += w_pt[i];
        gen++;
    }
    for (int i=1; i < m; i++) w[i] /= 1 << (m-i);
    return w;
}

/** Calculate the coefficients of the weight enumerator polynomial up to degree m and determine
    the t-value of the digital (t,m,s)-net given by the generating matrices in file: */
int t_value(unsigned& m, unsigned& s, std::string file)
{
    std::vector<cpp_int> p = poly_pts(m, s, file);
    std::vector<cpp_int> q = Q_m(m, s);
    std::vector<cpp_int> r(m+1,0);
    for (int i=0; i <= m; i++)
    {
        for (int j=0; j <= m-i; j++) {
            r[i+j] += p[i]*q[j];
        }
    }
    for (int a=1; a <= m; a++) {
        if (r[a] != 0) return m + 1 - a;
    }
    return 0;
}

/** Determine the t-value of the digital (t,m,s)-nets with m = 1,...,m_e which are
    given by the generating matrices in file in an embedded fashion: */
std::vector<int> t_value_embedded(unsigned& m_e, unsigned& s, std::string file)
{
    std::vector<int> t(m_e,0);
    std::vector<cpp_int> q = Q_m(m_e, s);
    std::vector<cpp_int> w(m_e+1,0); w[0] = 1;
    // Load file containing the generating matrices:
    std::istream* isptr(&std::cin);
    std::ifstream ifs;
    if(file.size()) {
        ifs.open(file);
        if(!ifs.is_open()) {
            std::cerr << " :: Error: couldn't open " << file << "\n";
            return t;
        }
        isptr = &ifs;
    }
    // Load generating matrices and construct generator:
    auto Cs = qmc::load_generating_matrices(*isptr, s, m_e);
    qmc::digitalseq_b2g<long double, std::uint64_t> gen(s, m_e, Cs.begin());
    gen.set_state(1);
    for (int m=1; m <= m_e; m++) {
        std::vector<cpp_int> w_m(m+1,0); w_m[0] = 1;
        std::vector<cpp_int> r_m(m+1,0);
        for (int l = 1 << (m-1); l < 1 << m; l++) {
            std::vector<cpp_int> w_pt(m_e+1,0); w_pt[0] = 1;
            for (int j=0; j < s; j++) {
                int msb = msb_pos((gen.cur[j] >> (64-m_e)) << (64-m_e));
                if (msb > 0){
                    for (int i=m_e; i >= 64-msb; i--) {
                        w_pt.at(i) -= w_pt.at(i+msb-64);
                    }
                }
            }
            for (int i=1; i <= m_e; i++) w[i] += w_pt[i];
            gen++;
        }
        for (int i=1; i <= m; i++) w_m[i] = w[i] / (1 << (m-i));
        for (int i=0; i <= m; i++)
        {
            for (int j=0; j <= m-i; j++) {
                r_m[i+j] += w_m[i] * q[j];
            }
        }
        for (int a=1; a <= m; a++) {
            if (r_m[a] != 0) {
                t[m-1] = m + 1 - a;
                break;
            }
        }
    }
    return t;
}

int main(int argc, char* argv[])
{
    bool write_to_file = 0;

    if (argc < 4) {
        std::cerr << "Call syntax: ./tval_digitalnet_b2 m_s m_e s generating_matrices_file" << std::endl;
    } else if (argc == 4) {
        unsigned int m = strtoul(argv[1], NULL, 0);
        unsigned int s = strtoul(argv[2], NULL, 0);
        std::string filename = argv[3];

        int t = t_value(m, s, filename);
        std::cout << '\n' << "The t-value of the given digital net is: " << t << '\n';
        std::cout << "The strength m - t of the given digital net is: " << m - t << '\n';
    } else if (argc == 5) {
        unsigned int m_s = strtoul(argv[1], NULL, 0);
        unsigned int m_e = strtoul(argv[2], NULL, 0);
        unsigned int s = strtoul(argv[3], NULL, 0);
        std::string filename = argv[4];
        std::cerr << "Loading digital net from: " << filename << std::endl;

        std::vector<int> t = t_value_embedded(m_e, s, filename);
        std::cout << '\n' << "The computed t-values of the given digital net for s = " << s << " with m = " << m_s << " to m = " << m_e << " are:" << '\n';
        std::cout << '\n' << "m" << '\t' << "t_value" << '\t' << "strength" << '\n';
        for (int i=0; i <= m_e-m_s; i++) {
            std::cout << (m_s+i) << '\t' << t[m_s+i-1] << '\t' << (m_s+i-t[m_s+i-1]) << '\n';
        }
    } else if (argc == 6) {
        unsigned int m_s = strtoul(argv[1], NULL, 0);
        unsigned int m_e = strtoul(argv[2], NULL, 0);
        unsigned int s_start = strtoul(argv[3], NULL, 0);
        unsigned int s_end = strtoul(argv[4], NULL, 0);
        std::string filename = argv[5];
        std::cerr << "Loading digital net from: " << filename << std::endl;

        std::cout << '\n' << "The computed t-values for m = " << m_s << " to " << m_e << " and s = " << s_start << " to " << s_end << " are:" << '\n';
        std::cout << '\n' << "s\\m";
        for (int i=0; i <= m_e-m_s; i++) std::cout << '\t' << m_s + i;
        std::cout << '\n';
        for (unsigned int s=s_start; s <= s_end; s++) {
            std::vector<int> t = t_value_embedded(m_e, s, filename);
            std::cout << '\n' << s;
            for (int i=0; i <= m_e-m_s; i++) std::cout << '\t' << t[m_s+i-1];
        }
        std::cout << '\n';

        if (write_to_file) {
            std::string save_file = "net_t-values.txt";
            std::ofstream t_values;
            t_values.open(save_file);

            for (unsigned int s=s_start; s <= s_end; s++) {
                std::vector<int> t = t_value_embedded(m_e, s, filename);
                for (int i=0; i <= m_e-m_s; i++) t_values << t[m_s+i-1] << '\t';
                t_values << '\n';
            }
            t_values.close();
        }
    }
}
