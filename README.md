## Quasi-Monte Carlo integration and rank-1 lattice rules

This repository contains construction algorithms for the generation of QMC point sets in Matlab and Python. In particular, we provide component-by-component type algorithms for the
construction of good generating vectors for rank-1 lattice rules. Furthermore, we provide construction schemes for digital nets and sequences.

Quasi-Monte Carlo rules are equal-weight cubature rules of the form

![QMC rule](https://latex.codecogs.com/gif.download?%5Cdpi%7B120%7D%20%5Cqquad%20%5Cqquad%20Q_N%28f%29%20%3A%3D%20%5Cfrac%7B1%7D%7BN%7D%20%5Csum_%7Bk%3D0%7D%5E%7BN-1%7D%20f%28%5Cmathbf%7Bx%7D_k%29%20%2C)

where the underlying points belong to a QMC point set, which are used to approximate high-dimensional integrals

![Integral](https://latex.codecogs.com/gif.download?%5Cdpi%7B120%7D%20I%28f%29%20%3A%3D%20%5Cint_%7B%5B0%2C1%5D%5Es%7D%20f%28%5Cmathbf%7Bx%7D%29%20%5C%2C%20%5Cmathrm%7Bd%7D%20%5Cmathbf%7Bx%7D)

over the s-dimensional unit cube. A special family of QMC point sets are so-called rank-1 lattice rules for which the underlying point set P is of the form 

![Lattice rule](https://latex.codecogs.com/gif.download?%5Cdpi%7B120%7D%20P%20%3D%20%5Cleft%5C%7B%20%5Cfrac%7Bk%20%5Cmathbf%7Bz%7D%20%5Cbmod%20N%7D%7BN%7D%20%5C%2C%5C%2C%5Cbigg%7C%5C%2C%5C%2C%200%20%5Cle%20k%20%3C%20N%20%5Cright%5C%7D)

with integer generating vectors z of length s.

---

## Contents

This repository contains Matlab and Python code for the construction of QMC point sets. In particular:

- Component-by-component-type constructions algorithms for lattice rules
- Algorithms for the construction of generating matrices for digital nets and sequences
- Accompanying programs and scripts for my doctoral thesis

---

## Acknowledging

If you use any of the provided code for your research, please be so kind to cite one of the corresponding articles below:

- A. Ebert, H. Leövey, and D. Nuyens. Successive coordinate search and component-by-component construction of rank-1 lattice rules. *Monte Carlo and Quasi–Monte Carlo Methods 2016*, 197–215, 2018
- A. Ebert and P. Kritzer. Constructing lattice points for numerical integration by a reduced fast successive coordinate search algorithm. *Journal of Computational and Applied Mathematics*, 351:77–100, 2019
- A. Ebert, P. Kritzer, and D. Nuyens. Constructing QMC finite element methods for elliptic PDEs with random coefficients by a reduced CBC construction. *Monte Carlo and Quasi–Monte Carlo Methods 2018*, To appear in 2020.
- A. Ebert, P. Kritzer, D. Nuyens, and Onyekachi Osisiogu. Digit-by-digit and component-by-component constructions of lattice rules for periodic functions with unknown smoothness. Available on [arXiv](https://arxiv.org/abs/2001.02978), 2020

---

Copyright: Adrian Ebert, <adrian.ebert@cs.kuleuven.be> / <adrian.ebert@oeaw.ac.at> , KU Leuven, 2020