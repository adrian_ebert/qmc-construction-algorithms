from math import pi as float_pi

"""
  The precomputed twidle factors for the fft.
  These are the roots of -1.
  This is a double hash. The first key is a stringification of pi up to the
  precision used.
  The second key is sign*log2(n). (sign=-1 for fft, and sign=+1 for ifft)
  (Actually if w are the twiddles for sign=-1, then conjugate(w) are the
  twiddles for sign=+1.)

  The following changes were made by Adrian Ebert:
  - Changed to 'ValueError, "..."' to 'ValueError("...")' to avoid SyntaxError
  - Changed "t = f[:(n/2)].copy()" to "t = f[:(n//2)].copy()" to avoid TypeError
  - Replaced ".has_key(...)" command by "in" command (the former was replaced by "in" in Python 3)
  - When integer result was required division with "/" was changed to integer division "//"
"""
fft_twidle_base2_table = {}

def get_fft_twidle_base2(m, sign, Pi=float_pi):
  from numpy import exp, arange
  if str(Pi) not in fft_twidle_base2_table:
    fft_twidle_base2_table[str(Pi)] = {}
  table = fft_twidle_base2_table[str(Pi)]
  k = sign*m
  if k in table: return table[k]
  n = 2**m
  table[k] = exp((sign*2j*Pi / n) * arange(n/2))
  return table[k]

def fft_base2(f, sign=-1, Pi=float_pi, scaling="Matlab"):
  """
    Calculate the FFT in base 2. Possibly in multiprecision.

    input:
      f         f should be a one-dimensional numpy.array with length a power
                of 2 and ***already be of complex type***
      sign      -1 for fft
                +1 for fft, or whatever convention you want to use
      Pi        the number representing pi, the default is just math.pi,
                however you could use mpmath.pi() upto arbitrary floating
                point precicion; in that case also f should be of this
                precision, being an array with mpmath.mpc objects
      scaling   one of "Matlab", "Unitary" or "None"
                  Matlab: no scaling for fft (sign=-1), 1/n scaling for ifft (sign=+1)
                  Unitary: scale both fft and ifft with 1/sqrt(n)
                  None: do not scale at all
    output:
      F         the fft or ifft of f

    Note: to use multiprecision objects like those from mpmath you need to
    attach the multiprecision complex class with an exp method and a sqrt
    method (the latter is only needed if you want to use scaling="Unitary").
    E.g.:
        >>> import myfft, mpmath, numpy
        >>> mpmath.mpc.exp = mpmath.exp; mpmath.mpc.sqrt = mpmath.sqrt; mpmath.mp.dps = 20
        >>> n = 2**10
        >>> x = numpy.array([ mpmath.sin(mpmath.pi()*k/(n+1)) for k in range(n) ])
        >>> myfft.fft_base2(x, Pi=mpmath.pi())
        array([(652.53169088497334944 + 0.0j),
               (-216.94804862436939414 + 0.66558096216421674524j),
               (-43.41380692395255081 + 0.26636831951100336238j), ...,
               (-18.608467066659721211 - 0.17124681181321776899j),
               (-43.41380692395255081 - 0.26636831951100336247j),
               (-216.94804862436939414 - 0.66558096216421674594j)], dtype=object)

    Note: this code is adjusted from code found on the internet at
      http://starship.python.net/~hochberg/fourier.py.html
    Site was offline when rewriting, copy to be found at:
      http://web.archive.org/web/20060925091347/http://starship.python.net/~hochberg/fourier.py.html

    (c) Dirk Nuyens <dirk.nuyens@cs.kuleuven.be> while at UNSW
    Fri Nov 28 22:18:55 EST 2008 (Sydney)
  """
  from numpy import array, transpose, reshape, arange, zeros
  from numpy import multiply, subtract, add, log, exp, pi, ravel, log2
  from copy import deepcopy
  if sign != -1 and sign != 1:
    raise ValueError("sign can only be -1 (fft) or +1 (ifft)")
  n = len(f)
  m = int(log2(n))
  if n != 2**m:
    raise ValueError("Can only do powers of 2, sorry...")
  if m == 0:
    return f.copy()
  #t = zeros(n/2, dtype=f.dtype)
  t = f[:(n//2)].copy()
  # Bit reverse f.
  f = transpose(reshape(f, (2,)*m))
  # Build up the fft.
  step = 2
  w = get_fft_twidle_base2(m, sign, Pi)
  while n > step/2:
    f = reshape(f, (n//step, 2, step//2))
    t = reshape(t, (n//step, step//2))
    multiply(w[::n//step], f[:,1,:], t)
    subtract(f[:,0,:], t, f[:,1,:])
    add(     f[:,0,:], t, f[:,0,:])
    step = step * 2
  if sign == 1 and scaling == "Matlab":
    return ravel(f) / n
  if scaling == "Unitary":
    # to provide the correct precision for sqrt(n) we do some trickery
    # (this works because numpy looks for specific functions as class methods
    # and we expect you to extend the class methods with exp and sqrt for
    # mpmath objects)
    N = numpy.array([(f[0] - f[0]) + n])
    return ravel(f) / numpy.sqrt(N[0])
  return ravel(f)

def fft(x):
  return myfft.fft_base2(x, sign=-1)

def ifft(x):
  return myfft.fft_base2(x, sign=+1)

import mpmath
mpmath.mpc.exp = mpmath.exp
mpmath.mpf.sqrt = mpmath.sqrt

def fft_mp(x):
  return fft_base2(x, sign=-1, Pi=mpmath.pi())

def ifft_mp(x):
  return fft_base2(x, sign=+1, Pi=mpmath.pi())

def _test():
  import doctest
  doctest.testmod()

if __name__ == "__main__":
  _test()
