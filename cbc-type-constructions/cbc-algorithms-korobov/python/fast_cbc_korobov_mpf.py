'''
# function z = cbc_korobov(N, s, gamma)
#
# inputs
#   N           number of points N, integer, must be prime
#   s           number of dimensions, scalar
#   gamma       product weight sequence per dimension, array of length s
#   power_2_fft boolean to force that FFT is of size power of 2, optional, defaults to False
#
# outputs
#   z           generating vector of the lattice rule, array of length s
#
# Example use:
#   mp.dps = 60
#   N = 4001; s = 10; gamma = mpf('0.7')**(np.arange(1,s+1));
#   z = cbc_korobov_mpf(N, s, gamma)
#
#   Based on the Matlab implementation of the fast CBC algorithm by Dirk Nuyens
'''

import numpy as np
import sympy as sy
import mpmath
import myfft

from mpmath import mp, mpf, mpc

# Determine next highest power of two for 32-bit integer:
def next_pow_2(n):
    n -= 1
    n |= n >> 1
    n |= n >> 2
    n |= n >> 4
    n |= n >> 8
    n |= n >> 16
    n += 1
    return n

def prime_factors(n):
    i = 2
    factors = []
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
            factors.append(i)
    if n > 1:
        factors.append(n)
    return factors

def generatorp(p):
    if sy.isprime(p) == False:
        return 'The value of p must be prime'
    primef = np.unique(prime_factors(p-1))
    g = 2; i = 0;
    while i < len(primef):
        if pow(g, int((p-1) // primef[i]), p) == 1:
            g += 1; i = -1;
        i += 1
    return [g, primef[-1]]

# Create vectorized functions for multi-precision:
real_vec = np.vectorize(np.real)
mp_sin_vec = np.vectorize(mpmath.sin)
mp_log_vec = np.vectorize(mpmath.log)

def cbc_korobov_mpf(N, s, gamma):
    if sy.isprime(N) == False:
        return 'The value of n must be prime'
    z = np.ones(s, dtype=int)
    m = (N-1) // 2
    gf = generatorp(N); g = gf[0];
    L = next_pow_2(m) << 1

    perm = np.ones(m, dtype=int)
    for j in range(m-1):
        perm[j+1] = np.mod(perm[j]*g, N)
    perm = np.minimum(N - perm, perm)
    psi = -2*mp_log_vec(2*mp_sin_vec(mpmath.pi*perm/mpf(N)))
    fft_psi = np.zeros(L, dtype=mpc)
    fft_psi[:m] = psi; fft_psi[-m:] = psi;
    fft_psi = myfft.fft_mp(fft_psi)

    q = mpf('0') + np.zeros(L); q[:m] = mpf('1');
    for j in range(s):
        E2 = real_vec( myfft.ifft_mp(fft_psi * myfft.fft_mp(np.array(q)))[:m] )
        w = np.argmin(E2)
        if j == 0: w = 0
        z[j] = perm[w]
        idx = np.concatenate((np.arange(w,-1,-1), np.arange(m-1,w,-1)))
        q[:m] *= 1 + gamma[j] * psi[idx]
    return z
