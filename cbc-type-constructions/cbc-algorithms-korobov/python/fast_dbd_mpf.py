#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 18 16:21:19 2019

@author: oosisiogu, Adrian Ebert
"""
'''
dbd_cbc_mpf(n, s, gamma):

inputs
    n           number of points 2^n, integer, scalar
    s           number of dimensions, integer, scalar
    gamma       product weight sequence per dimension, array of length s
    high_prec   boolean, 1 for high precision calculation

outputs
    z           generating vector of the lattice rule, array of length s

example use
    Construct lattice rule with n = 10, gamma_j = j^(-2), s = 20 using higher precision:

    mp.dps = 60
    n = 10; s = 20;
    gamma = mpf('1.0')/pow(np.arange(1,s+1),2)
    z = dbd_cbc_mpf(n, s, gamma, True)
'''

import numpy as np
import mpmath
from mpmath import mp, mpf, pi

def dbd_cbc_mpf(n, s, gamma, high_prec=False):
    assert len(gamma) >= s, "Provided weight sequence not long enough"
    if high_prec:
        K = np.array([-mpmath.log(mpmath.sin(pi*i/2**n)**2) for i in range(1,2**(n-1))], object)
    else:
        K = -np.log(np.sin(np.pi*np.arange(1,2**(n-1))/2**n)**2)
    q = 1 + gamma[0]*K
    z = np.ones(s, dtype=int)
    for r in range(1,s):
        for v in range(2,n+1):
            z_rv = z[r] + np.array([[0],[2**(v-1)]])
            idx = np.mod(np.multiply(z_rv,np.arange(1,2**(v-1),2)),2**v)
            idx = 2**(n-v)*np.minimum(idx,2**v - idx)
            u = K[idx-1]
            h_rv = np.zeros(2, object)
            for k in range(v,n+1):
                q_idx = 2**(n-k) * np.arange(1,2**(k-1),2).reshape(-1,2**(v-2))
                q_idx[1::2,:] = np.fliplr(q_idx[1::2,:])
                h_rv += 2**(v-k+1)*((q[q_idx-1].sum(axis=0)*u).sum(axis=1))
            w = np.argmin(h_rv)
            z[r] = z_rv[w]
            idx = np.mod(z[r] * np.arange(1,2**(v-1),2), 2**v)
            idx = 2**(n-v)*np.minimum(idx, 2**v-idx)
            u = K[idx-1]
            q[2**(n-v)*np.arange(1,2**(v-1),2)-1] *= 1 + gamma[r]*u
    return z
