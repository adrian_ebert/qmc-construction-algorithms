'''
function z = cbc_pow2_mpf(n, s, omega, gamma, beta)

 inputs
   n           number of points 2^n, integer, scalar
   s           number of dimensions, scalar
   omega       function handle for the varying kernel part of the
               shift-invariant kernel function (assumed to be symmetric on [0,1])
   gamma       product weight sequence per dimension, array of length s
   beta        constant weights sequence per dimension, array of length s

 outputs
   z           generating vector of the lattice rule, array of length s

 Example use:
    mp.dps = 60
    n = 6; s = 10;
    omega = lambda x: 2*(pi**2)*(x**2 - x + mpf('1')/6)
    gamma = mpf('0.7')**(np.arange(1,s+1)); beta = mpf('0') + np.ones(s);
    z = cbc_pow2_mpf(n, s, omega, gamma, beta)
'''

import numpy as np
import mpmath
from mpmath import mp, mpf, mpc, pi

import myfft
from numpy.matlib import repmat

real_vec = np.vectorize(np.real)

def cbc_pow2_mpf(n, s, omega, gamma, beta):
    assert n >= 1, "The value n must be greater than or equal to 1"
    assert len(gamma) >= s, "Provided weight sequence not long enough"
    N = 2**n
    phi = np.ones(n, dtype=int)
    for k in range(2,n):
        phi[k] = 2**(k-1)
    perm = np.ones(phi[-1], dtype=int)
    for i in range(1,phi[-1]):
        perm[i] = pow(5, i, N)
    # perm = 5 ** np.arange(phi[-1]) % N
    if n != 1:
        sidx = np.concatenate(([0], np.cumsum(phi[np.arange(0,n-1)])))
    else:
        sidx = 0
    eidx = sidx + phi
    psi = np.zeros(np.sum(phi), dtype=mpf)
    fft_psi = np.zeros(np.sum(phi), dtype=mpc)
    for k in range(n):
        psi[sidx[k]:eidx[k]] = omega(np.mod(perm[:phi[k]], 2**(k+1)) / mpf(2**(k+1)))
        fft_psi[sidx[k]:eidx[k]] = myfft.fft_mp(np.array(psi[sidx[k]:eidx[k]]))
    z = np.zeros(s, dtype=int)
    E2 = mpf('0') + np.zeros(phi[-1])
    q = mpf('0') + np.ones(np.sum(phi))
    for j in range(s):
        r = 1; v = 1;
        for k in range(n):
            E2[:phi[k]] = repmat(E2[:v],1,phi[k]//v) + gamma[j] * r * real_vec(myfft.ifft_mp(fft_psi[sidx[k]:eidx[k]] \
                                                                             * myfft.fft_mp(np.array(q[sidx[k]:eidx[k]]))))
            r = 2; v = phi[k];
        w = np.argmin(E2)
        if j == 0:
            w = 0
        z[j] = perm[w]
        z[j] = np.minimum(z[j], N - z[j])
        for k in range(n):
            u = np.mod(w, phi[k]) + 1
            idx = np.concatenate((np.arange(u,0,-1),np.arange(phi[k],u,-1)))
            q[sidx[k]:eidx[k]] = (beta[j] + gamma[j]*psi[idx+sidx[k]-1]) * q[sidx[k]:eidx[k]]
    return z
