"""
Created on Mon Apr 1 17:14:32 2019

@author: Adrian Ebert
"""
'''
wce_kor_pow2(n, z, gamma, alpha, fl_dtype=np.float):

inputs
    n           number of points 2^n, integer, scalar
    z           generating vector of the lattice rule, integer array of length s
    gamma       product weight sequence per dimension, array of length s
    alpha       smoothness parameter corresponding to the Korobov space
                fast computation for alpha = 2 or 4, reasonably fast computation for odd alpha, otherwise slow computation
    fl_dtype    floating point time, optional, defaults to np.float, other options: mpmath.mpf

outputs
    E           worst-case error of the lattice rule with 2^n points for the specified Korobov space

example use
    Calculate worst-case error for lattice rule with generator z and N = 2^16 points
    for the Korobov space (L_inf) of smoothness alpha=2, gamma_j = j^(-2) using high-precision floats:

    fl_dtype = mpmath.mpf
    mp.dps = 30

    n = 16
    z = np.array([1, 39725, 40293, 59973, 10613, 42149, 46237])
    gamma = fl_dtype(1)/np.power(np.arange(1,len(z)+1),2)

    E = wce_kor_pow2(n, z, gamma, 2, fl_dtype)
'''

import numpy as np
import mpmath
from mpmath import mp, mpf

def T(alpha, x, fl_dtype=np.float):
    val = mpmath.fp.polylog(alpha, mpmath.fp.exp(2*mpmath.fp.pi*1j*x))
    val += mpmath.fp.polylog(alpha, mpmath.fp.exp(-2*mpmath.fp.pi*1j*x))
    return fl_dtype(mpmath.re(val))
# Vectorize the function T:
Tv = np.vectorize(T)

def wce_kor_pow2(n, z, gamma, alpha, fl_dtype=np.float):
    s = len(z); N = 2**n;
    assert len(gamma) >= s, "Provided weight sequence not long enough"
    X = np.hstack((0, np.arange(fl_dtype('1'),2**(n-1))/N, 0.5))
    if alpha == 2:
        omega = lambda x: 2*(fl_dtype(mpmath.pi)**2)*(x**2 - x + fl_dtype('1')/6)
        K = omega(X)
    elif alpha == 4:
        omega = lambda x: -(fl_dtype('2')/3)*(fl_dtype(mpmath.pi)**4)*(x**4 - 2*(x**3) + x**2 - fl_dtype('1')/30)
        K = omega(X)
    else:
        K = Tv(mpf(alpha), X, fl_dtype)
    q = np.ones(2**(n-1)+1, object)
    for j in range(s):
        A = np.mod(z[j] * np.arange(2**(n-1)+1), N)
        A = np.minimum(A, N-A)
        q *= 1 + gamma[j]*K[A]
    return -1 + (q[0] + 2*np.sum(q[1:-1]) + q[-1])/N

def wce_kor_naiv(n, z, gamma, omega):
    s = len(z); N = 2**n;
    assert len(gamma) >= s, "Provided weight sequence not long enough"
    q = np.ones(N)
    for j in range(s):
        A = np.mod(z[j] * np.arange(0,N),N)
        q *= 1 + gamma[j]*omega(A/N)
    return -1 + np.sum(q)/N

def wce_kor(N, z, gamma, alpha, fl_dtype=np.float):
    s = len(z);
    assert len(gamma) >= s, "Provided weight sequence not long enough"
    m = N // 2
    X = np.hstack((0, np.arange(fl_dtype('1'),m+1)/N))
    if alpha == 2:
        omega = lambda x: 2*(fl_dtype(mpmath.pi)**2)*(x**2 - x + fl_dtype('1')/6)
        K = omega(X)
    elif alpha == 4:
        omega = lambda x: -(fl_dtype('2')/3)*(fl_dtype(mpmath.pi)**4)*(x**4 - 2*(x**3) + x**2 - fl_dtype('1')/30)
        K = omega(X)
    else:
        K = Tv(mpf(alpha), X, fl_dtype)
    q = np.ones(m+1, object)
    for j in range(s):
        A = np.mod(z[j] * np.arange(m+1), N)
        A = np.minimum(A, N-A)
        q *= 1 + gamma[j]*K[A]
    E = (q[0] + 2*np.sum(q[1:]))/N
    if (N % 2) == 0: E -= q[-1]/N
    return E-1
