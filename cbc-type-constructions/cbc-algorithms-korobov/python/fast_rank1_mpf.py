'''
# function z = fast_rank1_mpf(N, s, omega, gamma, beta)
#
# inputs
#   N           number of points N, integer, must be prime
#   s           number of dimensions, scalar
#   omega       function handle for the varying kernel part of the
#               shift-invariant kernel function (assumed to be symmetric on [0,1])
#   gamma       product weight sequence per dimension, array of length s
#   beta        constant weights sequence per dimension, array of length s
#
# outputs
#   z           generating vector of the lattice rule, array of length s
#
# Example use:
#   mp.dps = 60
#   N = 4001; s = 10;
#   omega = lambda x: 2*(pi**2)*(x**2 - x + mpf('1')/6)
#   gamma = mpf('0.7')**(np.arange(1,s+1)); beta = mpf('0') + np.ones(s);
#   z = fast_rank1_mpf(N, s, omega, gamma, beta)
#
#   Based on the Matlab implementation of the fast CBC algorithm by Dirk Nuyens
'''

import numpy as np
import sympy as sy
import mpmath
import myfft

from mpmath import mp, mpf, mpc, pi

# Determine next highest power of two for 32-bit integer:
def next_pow_2(n):
    n -= 1
    n |= n >> 1
    n |= n >> 2
    n |= n >> 4
    n |= n >> 8
    n |= n >> 16
    n += 1
    return n

def prime_factors(n):
    i = 2
    factors = []
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
            factors.append(i)
    if n > 1:
        factors.append(n)
    return factors

def generatorp(p):
    if sy.isprime(p) == False:
        return 'The value of p must be prime'
    primef = np.unique(prime_factors(p-1))
    g = 2; i = 0;
    while i < len(primef):
        if pow(g, int((p-1) // primef[i]), p) == 1:
            g += 1; i = -1;
        i += 1
    return [g, primef[-1]]

real_vec = np.vectorize(np.real)

def fast_rank1_mpf(N, s, omega, gamma, beta):
    if sy.isprime(N) == False:
        return 'The value of n must be prime'
    z = np.zeros(s, dtype=int)
    m = (N-1) // 2
    gf = generatorp(N); g = gf[0];
    L = next_pow_2(m) << 1

    perm = np.ones(m, dtype=int)
    for j in range(m-1):
        perm[j+1] = np.mod(perm[j]*g, N)
    perm = np.minimum(N - perm, perm)
    psi = omega(perm / mpf(N))
    fft_psi = np.zeros(L, dtype=mpc)
    fft_psi[:m] = psi; fft_psi[-m:] = psi
    fft_psi = myfft.fft_mp(fft_psi)

    q = mpf('0') + np.zeros(L); q[:m] = mpf('1');
    for j in range(s):
        E2 = real_vec( myfft.ifft_mp(fft_psi * myfft.fft_mp(np.array(q)))[:m] )
        w = np.argmin(E2)
        if j == 0: w = 0
        z[j] = perm[w]
        idx = np.concatenate((np.arange(w,-1,-1), np.arange(m-1,w,-1)))
        q[:m] *= beta[j] + gamma[j] * psi[idx]
    return z
