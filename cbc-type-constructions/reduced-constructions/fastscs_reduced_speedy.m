% function z= fastcbc_reduced_speedy(p, m, s, omega, gamma, beta, w)
%
% inputs
%   p           prime number p, scalar, must be prime
%   m           number of points given by p^m, scalar
%   z0          initial vector with components u s.t. gcd(u,p^m) = 1, vector [s x 1]
%   omega       function handle for the varying kernel part of the
%               shift-invariant kernel function (assumed to be symmetric on [0,1])
%   gamma       product weights per dimension, vector [s x 1]
%   beta        constant weights per dimension, vector [s x 1]
%   w           ascending reduction weights per dimension, integer vector [s x 1] 
%
% outputs
%   z           generating vector of the lattice rule, integer vector [s x 1]
%
% Example use:
%   Construct a lattice rule using the reduced fast component-by-component 
%   construction for the Korobov space with alpha = 2, gamma = (0.7)^j, beta = 1:
%
%   p = 3; m = 7; s = 10; omega = @(x) 2*pi^2*(x.^2-x+1/6);
%   z0 = ones(s,1); gamma = (0.7).^(1:s); beta = ones(1,s); w = zeros(1,s);
%   z = fastscs_reduced_speedy(p, m, z0, omega, gamma, beta, w)

function [z, e2] = fastscs_reduced_speedy(p, m, z0, omega, gamma, beta, w)

if ~isprime(p), error('p must be prime'); end
if (m < 1), error('must have m >= 1'); end
if (w(1) < 0), error('must have w_j >= 0'); end

s = length(z0); n = p^m;                            % number of dimensions s and points n = p^m

if p == 2, g = 5; else, g = generator(p*p); end     % any generator for mod p^2 is also one for p^m (m > 2)
phi = zeros(m,1);                                   % calculate Euler's totient function for i = 1,...,t
phi(1) = p-1; for k=2:m, phi(k) = p*phi(k-1); end  
phi(phi >= 2) = phi(phi >= 2)/2;                    % divide by 2 due to the symmetry w.r.t. omega

perm = zeros(phi(m),1); perm(1) = 1;                % g^i mod p^m sequence (cyclic group in generator order)
for i=2:phi(m), perm(i) = mod(perm(i-1)*g, n); end
if m~=1, sidx = [1; cumsum(phi(1:m-1))+1]; else, sidx = 1; end
eidx = sidx + phi - 1;                              % calculate start and end indices

psi0 = omega(0); psi = zeros(sum(phi),1);           % build the psi vector and its FFT
fft_psi = zeros(sum(phi),1);                        % this corresponds to the M_p^k matrices
for k=1:m
  psi(sidx(k):eidx(k)) = omega( mod(perm(1:phi(k)), p^k)/p^k );
  fft_psi(sidx(k):eidx(k)) = fft(psi(sidx(k):eidx(k)));
end

q = ones(sum(phi),1); w0 = zeros(size(z0));
s_star = find(w < m,1,'last');
z0_red = z0(1:s_star) ./ (p.^w(1:s_star));           % Calculate reduced components of z0

for j=1:s_star
  t = m - w(j);
  perm_red = mod(perm(1:phi(t)),p^t);
  if ~ismember(z0_red(j),perm_red), z0_red(j) = mod(-z0_red(j),p^t); end
  w0(j) = find(perm_red == z0_red(j));
  for k=1:t
    u = mod(w0(j)-1, phi(k)) + 1;
    if (p == 2 && k == 1)
    q(sidx(w(j)+k):eidx(w(j)+k)) = (beta(j) + gamma(j)*psi([u:-1:1 phi(k):-1:u+1]+sidx(k)-1)) ...
                                .* q(sidx(w(j)+k):eidx(w(j)+k));
    else    
    q(sidx(w(j)+k):eidx(w(j)+k)) = (beta(j) + gamma(j)*repmat(psi([u:-1:1 phi(k):-1:u+1]+sidx(k)-1),[p^w(j),1])) ...
                                .* q(sidx(w(j)+k):eidx(w(j)+k));
    end
  end
  for k=1:w(j)
    q(sidx(k):eidx(k)) = (beta(j) + gamma(j)*psi0) * q(sidx(k):eidx(k)); 
  end
end
q = prod(beta(s_star+1:s) + gamma(s_star+1:s)*psi0) * q;

z = ones(s,1); e2 = zeros(s,1); 
for j=1:s
  Y = p^w(j); t = m - w(j);                         % calculate multiplicator Y_j and reduced dimension t
  if (t > 0)
      for k=1:t
        u = mod(w0(j)-1, phi(k)) + 1;
        if (p == 2 && k == 1)
            q(sidx(w(j)+k):eidx(w(j)+k)) = q(sidx(w(j)+k):eidx(w(j)+k)) ...
            ./ (beta(j) + gamma(j)*psi([u:-1:1 phi(k):-1:u+1]+sidx(k)-1));
        else    
            q(sidx(w(j)+k):eidx(w(j)+k)) = q(sidx(w(j)+k):eidx(w(j)+k)) ...
            ./ (beta(j) + gamma(j)*repmat(psi([u:-1:1 phi(k):-1:u+1]+sidx(k)-1),[p^w(j),1]));
        end
      end      
      E2 = zeros(phi(t),1); v = 1;
      if p == 2, r = 1; else, r = 2; end 
      for k=1:t
        if (p == 2 && k == 1)
            q_temp = sum(q(sidx(w(j)+k):eidx(w(j)+k)));
        else
            q_temp = sum(reshape(q(sidx(w(j)+k):eidx(w(j)+k)),[phi(w(j)+k)/p^w(j),p^w(j)]),2);
        end
        E2(1:phi(k)) = repmat(E2(1:v), phi(k)/v, 1) ...
                     + gamma(j) * r * real(ifft( fft_psi(sidx(k):eidx(k)) .* fft(q_temp) ));
        r = 2; v = phi(k);
      end
      [~,a] = min(E2);                                  % pick the best choice for a
      z(j) = mod(perm(a), p^t);                         % z_j = g^a in the reduced search space
      for k=1:t                                         % update q
        u = mod(a-1, phi(k)) + 1;
        if (p == 2 && k == 1)
            q(sidx(w(j)+k):eidx(w(j)+k)) = (beta(j) + gamma(j)*psi([u:-1:1 phi(k):-1:u+1]+sidx(k)-1)) ...
                                        .* q(sidx(w(j)+k):eidx(w(j)+k));
        else    
            q(sidx(w(j)+k):eidx(w(j)+k)) = (beta(j) + gamma(j)*repmat(psi([u:-1:1 phi(k):-1:u+1]+sidx(k)-1),[p^w(j),1])) ...
                                        .* q(sidx(w(j)+k):eidx(w(j)+k));
        end                        
      end
  end
  z(j) = mod(Y*z(j),n); z(j) = min(z(j),n-z(j));
end
end