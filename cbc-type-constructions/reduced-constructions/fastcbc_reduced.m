% function [z, e2] = fastcbc_reduced(p, m, s, omega, gamma, beta, w)
%
% inputs
%   p           prime number p, scalar, must be prime
%   m           number of points given by p^m, scalar
%   s           number of dimensions, scalar
%   omega       function handle for the varying kernel part of the
%               shift-invariant kernel function (assumed to be symmetric on [0,1])
%   gamma       product weights per dimension, vector [s x 1]
%   beta        constant weights per dimension, vector [s x 1]
%   w           ascending reduction weights per dimension, integer vector [s x 1] 
%
% outputs
%   z           generating vector of the lattice rule, integer vector [s x 1]
%   e2          worst-case error per dimension, vector [s x 1]
%
% Example use:
%   Construct a lattice rule using the reduced fast component-by-component 
%   construction for the Korobov space with alpha = 2, gamma = (0.7)^j, beta = 1:
%
%   p = 3; m = 7; s = 10; omega = @(x) 2*pi^2*(x.^2-x+1/6);
%   gamma = (0.7).^(1:s); beta = ones(s,1); w = zeros(s,1);
%   [z, e2] = fastcbc_reduced(p, m, s, omega, gamma, beta, w)

function [z, e2] = fastcbc_reduced(p, m, s, omega, gamma, beta, w)

if ~isprime(p), error('p must be prime'); end
if (m < 1), error('must have m >= 1'); end
if (w(1) < 0), error('must have w_j >= 0'); end

n = p^m;                                            % number of points n = p^m
cumbeta = cumprod(beta);                            % product of the beta per dimension

if p == 2, g = 5; else, g = generator(p*p); end     % any generator for mod p^2 is also one for p^m (m > 2)
phi = zeros(m,1);                                   % calculate Euler's totient function for i = 1,...,t
phi(1) = p-1; for k=2:m, phi(k) = p*phi(k-1); end  
phi(phi >= 2) = phi(phi >= 2)/2;                    % divide by 2 due to the symmetry w.r.t. omega

perm = zeros(phi(m),1); perm(1) = 1;                % g^i mod p^m sequence (cyclic group in generator order)
for i=2:phi(m), perm(i) = mod(perm(i-1)*g, n); end
if m~=1, sidx = [1; cumsum(phi(1:m-1))+1]; else, sidx = 1; end
eidx = sidx + phi - 1;                              % calculate start and end indices

psi0 = omega(0); psi = zeros(sum(phi),1);           % build the psi vector and its FFT
fft_psi = zeros(sum(phi),1);                        % this corresponds to the M_p^k matrices
for k=1:m
  psi(sidx(k):eidx(k)) = omega( mod(perm(1:phi(k)), p^k)/p^k );
  fft_psi(sidx(k):eidx(k)) = fft(psi(sidx(k):eidx(k)));
end

q0 = 1; q = ones(sum(phi),1); 
z = ones(s,1); e2 = zeros(s,1); 
for j=1:s
  Y = p^w(j); t = m - w(j);                         % calculate multiplicator Y_j and reduced dimension t
  if (t > 0)
      E2 = zeros(phi(t),1); v = 1;
      if p == 2, r = 1; else, r = 2; end 
      for k=1:t
        if (p == 2 && k == 1)
            q_temp = sum(q(sidx(w(j)+k):eidx(w(j)+k)));
        else
            q_temp = sum(reshape(q(sidx(w(j)+k):eidx(w(j)+k)),[phi(w(j)+k)/p^w(j),p^w(j)]),2);
        end
        E2(1:phi(k)) = repmat(E2(1:v), phi(k)/v, 1) ...
                     + gamma(j) * r * real(ifft( fft_psi(sidx(k):eidx(k)) .* fft(q_temp) ));
        r = 2; v = phi(k);
      end
      [~,a] = min(E2);                                  % pick the best choice for a
      if j == 1, a = 1; end
      z(j) = mod(perm(a), p^t);                         % z_j = g^a in the reduced search space
      for k=1:t                                         % update q
        u = mod(a-1, phi(k)) + 1;
        if (p == 2 && k == 1)
            q(sidx(w(j)+k):eidx(w(j)+k)) = (beta(j) + gamma(j)*psi([u:-1:1 phi(k):-1:u+1]+sidx(k)-1)) ...
                                        .* q(sidx(w(j)+k):eidx(w(j)+k));
        else    
            q(sidx(w(j)+k):eidx(w(j)+k)) = (beta(j) + gamma(j)*repmat(psi([u:-1:1 phi(k):-1:u+1]+sidx(k)-1),[p^w(j),1])) ...
                                        .* q(sidx(w(j)+k):eidx(w(j)+k));
        end                        
      end
      for k=1:w(j)
        q(sidx(k):eidx(k)) = (beta(j) + gamma(j)*psi0) * q(sidx(k):eidx(k)); 
      end
  else
      q = (beta(j) + gamma(j)*psi0) * q;
  end
  q0 = (beta(j) + gamma(j)*psi0) * q0;
  if p == 2, r = 1; else, r = 2; end
  e2(j) = -cumbeta(j) + (q0 + r*sum(q(sidx(1):eidx(1))) + 2*sum(q(sidx(2):eidx(m))))/n;
  z(j) = mod(Y*z(j),n); z(j) = min(z(j),n-z(j));
end
end