% function w = vm(k, f, m)
%
% Given two polynomials over Z_2, calculate the Laurent expansion of k(x) /
% f(x) up to x^{-m} and then evaluate this in 2.
% It is assumed that deg(f) = m and f(x) is irreducible, such that the
% coefficient for x^m is just 1.
function w = vm(k, f, m)

k = reshape(k, 1, length(k)); % just for vectorization
w = zeros(m, length(k), class(f));
for ell = 1:m
  w(ell,:) = bitget(k, m-ell+1); % bits are indexed from 1
  for j = 1:ell-1
    w(ell,:) = mod(w(ell,:) - w(j,:) * bitget(f, m-(ell-j)+1), 2);
  end;
end;
w = pow2(-[1:m]) * double(w);