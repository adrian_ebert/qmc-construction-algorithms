'''
@author: Adrian Ebert

cbc(m, s, gamma, qual_meas, power_2_fft):

inputs
    m               number of points given by 2^m, integer, scalar
    s               number of dimensions, integer, scalar
    gamma           product weight sequence per dimension, array of length s
    qual_meas       either a function handle for the used quality measure or the smoothness parameter of the weighted Walsh space
    power2_fft      boolean to force that FFT is of size power of 2, optional, defaults to True

outputs
    g           generating vector of the polynomial lattice rule, array of length s

example use
    Construct lattice rule with m = 10, gamma_j = j^(-2), s = 20:

    m = 10; s = 20;
    gamma = 1 / np.power(np.arange(1,s+1), 2)
    qual_meas = 2
    g = cbc(m, s, gamma, qual_meas)
'''

from poly_arithmetic import *

def cbc(m, s, gamma, qual_meas, power2_fft=True):
    assert len(gamma) >= s, 'Provided weight sequence not long enough'
    if callable(qual_meas):
        omega = qual_meas
    elif isinstance(qual_meas, int) or isinstance(qual_meas, float):
        alpha = qual_meas
        phi = lambda x: np.power(2, (alpha-1)*(np.floor(np.log2(x)) + 1))
        omega = lambda x: (1 - phi(x)) / (1 - 2**(1-alpha)) - phi(x)
    else:
        raise Exception('Unknown quality measure type given.')
    N = 2**m
    L = N << 1 if power2_fft else N-1
    g = np.zeros(s, dtype=int)
    perm = perm_gen(m)
    psi = omega(vm(perm, m, 'primitive'))
    fft_psi = np.zeros(L)
    fft_psi[:N-1] = psi
    if power2_fft: fft_psi[1-N:] = psi
    fft_psi = np.fft.fft(fft_psi)
    q = np.zeros(L); q[:N-1] = 1;
    for j in range(s):
        E2 = np.real( np.fft.ifft(fft_psi * np.fft.fft(q))[:N-1] )
        w = np.argmin(E2)
        if j == 0: w = 0
        g[j] = perm[w]
        idx = np.concatenate((np.arange(w,-1,-1), np.arange(N-2,w,-1)))
        q[:N-1] *= 1 + gamma[j] * psi[idx]
    return g
