'''
@author: Adrian Ebert

cbcdbd(m, s, gamma):

inputs
    m           number of points given by 2^m, integer, scalar
    s           number of dimensions, integer, scalar
    gamma       product weight sequence per dimension, array of length s
    fl_type     floating point type, optional, defaults to np.float, other options: mpf

outputs
    g           generating vector of the polynomial lattice rule, array of length s

example use
    Construct lattice rule with m = 10, gamma_j = j^(-2), s = 20:

    mp.dps = 50
    m = 10; s = 20;
    gamma = mpf(1)/pow(np.arange(1,s+1),2)
    fl_type = mpf
    g = cbcdbd(m, s, gamma, fl_type)
'''

from poly_arithmetic import *

def cbcdbd(m, s, gamma, fl_type=np.float):
    assert len(gamma) >= s, 'Provided weight sequence not long enough'
    N = 2**m
    g = np.ones(s, dtype=int)
    omega = lambda x: -(np.floor(np.log2(x)) + 1)
    K = np.hstack((omega(np.arange(1, N >> 1) / N), np.zeros(N >> 1)))
    q = 1 + gamma[0]*K
    for r in range(1,s):
        for w in range(2,m+1):
            g_rw = g[r] + np.array([0,2**(w-1)])
            a = poly_mult(g_rw[0], np.arange(1, 2**w, 2), w, 'monomial')
            b = poly_mult(g_rw[1], np.arange(1, 2**w, 2), w, 'monomial')
            idx = np.vstack((a, b))
            u = K[(2**(m-w))*idx - 1]
            h_rw = np.array([fl_type(0)] * 2)
            for l in range(w,m+1):
                q_idx = 2**(m-l) * np.arange(1, 2**l, 2)
                q_idx = np.reshape(q_idx, (-1, 2**(w-1)))
                h_rw += 2**(w-l) * ((q[q_idx-1].sum(axis=0) * u).sum(axis=1))
            v = np.argmin(h_rw)
            g[r] = g_rw[v]
            q[2**(m-w) * np.arange(1, 2**w, 2) - 1] *= 1 + gamma[r] * u[v]
    return g
