import numpy as np
import mpmath
from mpmath import mp, mpf, mpc, nstr

# The primitive polynomials p(x) are chosen such that for 1 <= deg(p) < 17, p matches the corresponding primitive polynomial used by
# Matlab's GF class. For deg(p) > 16, the polynomials are taken from https://www.partow.net/programming/polynomials/index.html#deg03.

prim_poly = {
    1:  3,
    2:  7,
    3:  11,
    4:  19,
    5:  37,
    6:  67,
    7:  137,
    8:  285,
    9:  529,
    10: 1033,
    11: 2053,
    12: 4179,
    13: 8219,
    14: 17475,
    15: 32771,
    16: 69643,
    17: 131081,
    18: 262273,
    19: 524327,
    20: 1048585,
    21: 2097157,
    22: 4194307,
    23: 8388641,
    24: 16777351,
    25: 33554441,
    26: 67108935,
    27: 134217767,
    28: 268435465,
    29: 536870917,
    30: 1082130439,
    31: 2147483657,
    32: 4299161607
}

# This function calculates the output of the function v_m(k/f) based on the Laurent expansion, where f(x) is either
# the monomial x^m or the given primitive polynomial of degree m.

def vm(k, m, f_type, fl_type=np.float):
    if f_type == "monomial":
        return np.mod(k, 2**m) / fl_type(2**m)
    elif f_type == "primitive":
        f = prim_poly[m]
        w = np.zeros((m, len(k)), dtype=int)
        for l in range(m):
            w[l,:] = (k >> (m-l-1)) & 1
            for j in range(l):
                w[l,:] = np.mod(w[l,:] - w[j,:] * ((f >> (m-l+j)) & 1), 2)
        res = w[m-1,:]
        for i in range(m-1):
            res ^= (w[i,:] << (m-1-i))
        return res / fl_type(2**m)
    else:
        raise Exception('Unknown polynomial type (f_type) given.')

# Calculate the product between the polynomials p(x) [scalar, integer] and q(x) [integer array]
# modulo x^m (for f_type = 'monomial') or modulo f with f primitive (for f_type = 'primitive').

def poly_mult(p, q, m, f_type):
    r = np.zeros(len(q), dtype=int)
    p = int(p) % 2**m; q %= 2**m;
    for i in range(int.bit_length(p)):
        if p & (1 << i):
            r ^= q << i
    if f_type == "monomial":
        return r % 2**m
    elif f_type == "primitive":
        # Create array of reduced powers x^m,...,x^(2m-1):
        f_red = np.zeros(m-1, dtype=int)
        f_red[0] = prim_poly[m] - 2**m
        for i in range(1, m-1):
            f_red[i] = f_red[i-1] << 1
            if f_red[i] & (1 << m):
                f_red[i] ^= prim_poly[m]
        # Reduce powers of obtained product via f_red:
        for i in range(m, 2*m-1):
            idx = r & (1 << i)
            r[idx > 0] ^= f_red[i-m] ^ (1 << i)
        return r
    else:
        raise Exception('Unknown polynomial type (f_type) given.')

# Calculate group of units modulo primitive f in generator order with a drastically reduced
# number of calls of the function poly_mult (to accelerate the involved algorithms).

def perm_gen(m):
    N = 2**m; cnt = m-1;
    perm = np.ones(N-1, dtype=int)
    perm[:m] = 1 << np.arange(m)
    while 2*cnt+1 <= N-1:
        perm[cnt+1:2*cnt+1] = poly_mult(perm[cnt], perm[1:cnt+1], m, 'primitive')
        cnt <<= 1
    perm[cnt+1:] = poly_mult(perm[cnt], perm[1:N-1-cnt], m, 'primitive')
    return perm
