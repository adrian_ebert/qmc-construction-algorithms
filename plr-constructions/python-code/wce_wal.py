'''
@author: Adrian Ebert

wce_wal(g, m, gamma, alpha, f_type, fl_type):

inputs
    g           generating vector of the polynomial lattice rule, integer array of length s
    m           number of points given by 2^m, integer, scalar
    gamma       product weight sequence per dimension, array of length s
    alpha       smoothness parameter corresponding to the Walsh space
    f_type      type of modulus polynomial, string, defaults to 'primitive', other options: 'monomial'
    fl_type    floating point type, optional, defaults to np.float, other options: mpf

outputs
    E           worst-case error of the polynomial lattice rule with 2^m points for the specified Walsh space

example use
    Calculate worst-case error for polynomial lattice rule with generating vector g and N = 2^16 points
    for the Walsh space (L_inf) of smoothness alpha = 2, gamma_j = j^(-2) using high-precision library:

    fl_type = mpmath.mpf
    mp.dps = 50

    m = 10
    g = np.array([1, 47, 293, 973, 613, 149, 37])
    gamma = fl_type(1) / np.power(np.arange(1,len(g)+1), 2)

    E = wce_wal(g, m, gamma, 2, 'primitive', fl_type)
'''

from poly_arithmetic import *

def wce_wal(g, m, gamma, alpha, f_type='primitive', fl_type=np.float):
    N = 2**m
    w = lambda x: fl_type(2) ** ((alpha-1)*(np.floor(np.log2(x)) + 1))
    phi = lambda x: (1 - w(x)) / (1 - 2**(1-alpha)) - w(x)
    K = vm(np.arange(1, N>>1), m, f_type)
    X = np.hstack((fl_type(1)/(1 - 2**(1-alpha)), phi(K), -fl_type(1) * np.ones(N>>1)))
    q = np.ones(N, object)
    if f_type == 'monomial' or f_type == 'primitive':
        for j in range(len(g)):
            idx = poly_mult(g[j], np.arange(N), m, f_type)
            q *= 1 + gamma[j] * X[idx]
        return -1 + fl_type(np.sum(q)) / N
    else:
        raise Exception('Unknown polynomial type (f_type) given.')
