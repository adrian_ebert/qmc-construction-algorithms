'''
@author: Adrian Ebert

cbc_mpf(m, s, gamma, qual_meas):

inputs
    m               number of points given by 2^m, integer, scalar
    s               number of dimensions, integer, scalar
    gamma           product weight sequence per dimension, array of length s
    qual_meas       either a function handle for the used quality measure (with mpf precision) or the smoothness parameter of the weighted Walsh space

outputs
    g               generating vector of the polynomial lattice rule, array of length s

example use
    Construct lattice rule with m = 10, gamma_j = j^(-2), s = 20:

    m = 10; s = 20; qual_meas = 2;
    gamma = mpf(1) / np.power(np.arange(1,s+1), 2)
    g = cbc_mpf(m, s, gamma, qual_meas)
'''

from poly_arithmetic import *
import myfft

real_vec = np.vectorize(np.real)

def cbc_mpf(m, s, gamma, qual_meas):
    assert len(gamma) >= s, 'Provided weight sequence not long enough'
    if callable(qual_meas):
        omega = qual_meas
    elif isinstance(qual_meas, int) or isinstance(qual_meas, float):
        alpha = mpf(str(qual_meas))
        phi = lambda x: np.power(mpf(2), (alpha-1)*(np.floor(np.log2(x)) + 1))
        omega = lambda x: (1 - phi(x)) / (1 - 2**(1-alpha)) - phi(x)
    else:
        raise Exception('Unknown quality measure type given.')
    N = 2**m
    L = N << 1
    g = np.zeros(s, dtype=int)
    perm = perm_gen(m)
    psi = omega(vm(perm, m, 'primitive'))
    fft_psi = np.zeros(L, dtype=mpc)
    fft_psi[:N-1] = psi; fft_psi[1-N:] = psi;
    fft_psi = myfft.fft_mp(fft_psi)
    q = mpf(0) + np.zeros(L); q[:N-1] = mpf(1);
    for j in range(s):
        E2 = real_vec( myfft.ifft_mp(fft_psi * myfft.fft_mp(q))[:N-1] )
        w = np.argmin(E2)
        if j == 0: w = 0
        g[j] = perm[w]
        idx = np.concatenate((np.arange(w,-1,-1), np.arange(N-2,w,-1)))
        q[:N-1] *= 1 + gamma[j] * psi[idx]
    return g
