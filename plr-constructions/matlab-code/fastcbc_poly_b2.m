% function [g, e2] = fastcbc_poly_b2(m, s, gamma, qual_meas)
%
% Polynomial lattice rule over Z_2/f with deg(f) = m.
%
% inputs
%   m           degree of the irreducible polynomial, integer, scalar
%   s           number of dimensions, integer, scalar
%   gamma       product weight parameters per dimension, vector [s x 1]
%   qual_meas   either a function handle for the used quality measure or
%               the smoothness parameter for the weighted Walsh space
%
% outputs
%   g           generating vector of the polynomial lattice rule, integer vector [s x 1]
%   e2          square worst-case error per dimension, vector [s x 1]
%
% example use
%
%   m = 10; s = 100;
%   u_wal = @(x) 2 - 6*pow2(floor(log2(x))); gamma = (1:s).^(-2);
%   [g, e2] = fastcbc_poly_b2(m, s, gamma, u_wal);
%
% Based on the Matlab implementation of Dirk Nuyens (KU Leuven)

function [g, e2] = fastcbc_poly_b2(m, s, gamma, qual_meas)

if isa(qual_meas, 'function_handle')
    omega = qual_meas;
else
    alpha = qual_meas;
    phi = @(x) pow2( (alpha-1)*(floor(log2(x)) + 1) );
    omega = @(x) (1 - phi(x)) / (1 - pow2(1-alpha)) - phi(x);
end    

g = zeros(s,1);
e2 = zeros(s, 1);
N = pow2(m);

gen = gf(2, m);   % generator gen = 2 = (10)_2 = x since f is primitive 
perm = gf(ones(N-1, 1), m);
for j=1:N-2, perm(j+1) = perm(j)*gen; end
psi = omega(vm(perm.x, m, 'irreducible')');
psi0 = omega(0);
fft_psi = fft(psi);

q = ones(N-1, 1);
q0 = 1;

for j=1:s
    E2 = real(ifft(fft_psi .* fft(q)));
    [min_E2, w] = min(E2);
    if j == 1, w = 1; end
    v = perm(w); g(j) = v.('x');
    e2(j) = -1 + (q0 + sum(q) + gamma(j)*(psi0*q0 + min_E2)) / N;
    q = (1 + gamma(j) * psi([w:-1:1 N-1:-1:w+1])) .* q;
    q0 = (1 + gamma(j) * psi0) * q0;
end