% function r = poly_mult(p, q, m)
%
% Given the polynomial p(x) over Z_2 with deg(p) < m and polynomials q(x) over Z_2, 
% calculate the product p(x) q(x) modulo x^m.
%
% inputs
%   p           polynomial p(x) over Z_2 with deg(p) < m, integer, scalar
%   q           polynomials q(x) over Z_2, integer vector
%   m           degree of the polynomial x^m, integer, scalar
%   
% outputs
%   r           product of p(x) and q(x) over Z_2 modulo x^m, integer vector

function r = poly_mult(p, q, m)

r = zeros(1, length(q));
p_bin = de2bi(mod(p, 2^m));
p_pos = find(p_bin) - 1;
q_red = mod(q, 2^m);

for i=1:length(p_pos)
    r = bitxor(r, bitshift(q_red, p_pos(i)));
end
r = mod(r, 2^m);