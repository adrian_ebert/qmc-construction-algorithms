% function w = vm(k, m, f_type, f)
%
% Given two polynomials over Z_2, calculate the Laurent expansion of k(x)/f(x) up to x^(-m) and then 
% evaluate it in x=2. We assume that f(x) is either a monomial or an irreducible polynomial, both with 
% deg(f) = m such that the coefficient for x^m is 1. The polynomials k(x) and f(x) are given by the 
% corresponding integer representation in base 2.
%
% inputs
%   k           integer vector representing the polynomials k(x) 
%   m           degree of the polynomial f(x), integer, scalar
%   f_type      type of f, either 'monomial' or 'irreducible', string
%   f           irreducible polynomial in GF(2^m), integer, scalar, optional
%   
% outputs
%   w           computed value of v_m(k(x)/f(x)), double vector
%
% Partially based on the Matlab implementation of Dirk Nuyens (KU Leuven)

function w = vm(k, m, f_type, f)

if strcmp(f_type,'monomial')
    w = double(mod(k(:)', pow2(m)))/pow2(m);
    if nargin == 4 
        fprintf('Polynomial input is being ignored.\n');
    end    
elseif strcmp(f_type,'irreducible')
    if nargin == 3 
        f = double(getfield(gf(1,m), 'prim_poly'));
    end
    k = k(:)';  % force k to be a row vector for vectorization
    w = zeros(m, length(k));
    for l=1:m
        w(l,:) = bitget(k, m-l+1);  % bits are indexed from 1
        for j=1:l-1
            w(l,:) = mod(w(l,:) - w(j,:)*bitget(f, m-(l-j)+1), 2);
        end
    end
    w = pow2(-(1:m)) * w;
else
    error('Unknown polynomial type given!');
end