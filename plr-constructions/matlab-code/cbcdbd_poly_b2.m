% function g = cbcdbd_poly_b2(m, s, gamma)
%
% Polynomial lattice rule over Z_2/f with deg(f) = m.
%
% inputs
%   m           degree of the polynomial x^m, integer, scalar
%   s           number of dimensions, integer, scalar
%   gamma       product weight parameters per dimension, vector [s x 1]
%
% outputs
%   g           generating vector of the polynomial lattice rule, integer vector [s x 1]
%
% example use
%
%   m = 10; s = 20; gamma = (1:s).^(-2);
%   g = cbcdbd_poly_b2(m, s, gamma);

function g = cbcdbd_poly_b2(m, s, gamma)

g = ones(s, 1);
N = pow2(m);
omega = @(x) -(floor(log2(x)) + 1);
K = [omega((1:N/2-1)/N), zeros(1, N/2)];
q = 1 + gamma(1)*K;

for r=2:s
    for w=2:m
        g_rw = g(r) + [0; pow2(w-1)];
        idx = [poly_mult(g_rw(1),1:2:2^w,w); poly_mult(g_rw(2),1:2:2^w,w)];
        u = K(pow2(m-w) * idx);
        h_rw = zeros(2,1);
        for l=w:m
            q_idx = pow2(m-l)*(1:2:2^l); 
            q_idx = reshape(q_idx, pow2(w-1), [])'; 
            h_rw = h_rw + pow2(w-l)*sum(sum(q(q_idx), 1) .* u, 2);
        end    
        [~,v] = min(h_rw);
        g(r) = g_rw(v);
        q(pow2(m-w)*(1:2:2^w)) = q(pow2(m-w)*(1:2:2^w)) .* (1 + gamma(r)*u(v,:));
    end
end