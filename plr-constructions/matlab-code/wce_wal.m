% function err = wce_wal(g, m, gamma, alpha, f_type)
%
% Calculation of the squared worst-case error of a polynomial lattice rule
% with 2^m points specified by the generating vector g in the weighted
% Walsh space with product weights gamma and smoothness alpha.
%
% inputs
%
%   g           generating vector of polynomial lattice rule, integer vector [s x 1]
%   m           number of points 2^m, integer, scalar
%   gamma       product weight parameters per dimension, vector [s x 1]
%   alpha       smoothness parameter of the weighted Walsh space
%   f_type      type of f, either 'monomial' or 'irreducible', string
%
% ouputs
%   err         worst-case error of the polynomial lattice rule with 2^m points
%
% Remark: If f_type = 'irreducible', we require that m <= 16 (gf library).

function err = wce_wal(g, m, gamma, alpha, f_type)

N = pow2(m);
w = @(x) pow2( (alpha-1)*(floor(log2(x)) + 1) );
phi = @(x) (1 - w(x)) / (1 - pow2(1-alpha)) - w(x);

K = vm((0:N/2-1), m, f_type);
X = [phi(K), -ones(1,N/2)];
q = ones(1, N);

if strcmp(f_type, 'monomial')
    for j = 1:length(g)
        idx = poly_mult(g(j), 0:N-1, m);
        q = (1 + gamma(j)*X(idx + 1)) .* q;
    end
elseif strcmp(f_type, 'irreducible')
    for j = 1:length(g)
        idx = g(j) * gf(0:N-1, m);
        q = (1 + gamma(j)*X(idx.x + 1)) .* q;
    end
else
    error('Unknown polynomial type given!');
end    
err = -1 + sum(q)/N;
end